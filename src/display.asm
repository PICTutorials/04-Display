;   Display - Play with a LED screen
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Display
;
;   Ths program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Display (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 1 - f
; - RB1 - 2 - g
; - RB2 - 6 - e
; - RB3 - 7 - d
; - RB4 - 8 - c
; - RB5 - 9 - DP
; - RB6 - 13 - b
; - RB7 - 14 - a
;
; 4 & 12 ==> GND's
;=============================

;Assembler directives
;=============================
	list p=16f628a
	include p16f628a.inc
	__config b'11111100111000'
	org 0x00
	goto Start

;Functions
;=============================
	include digitecnology.inc
Show9
        bsf PORTB,0
        bsf PORTB,1
        bcf PORTB,2
	bcf PORTB,3
	bsf PORTB,4
	bcf PORTB,5
	bsf PORTB,6
	bsf PORTB,7
        return
Show8
        bsf PORTB,0
        bsf PORTB,1
        bsf PORTB,2
        bsf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bsf PORTB,7
        return
Show7
        bcf PORTB,0
        bcf PORTB,1
        bcf PORTB,2
        bcf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bsf PORTB,7
        return
Show6
        bsf PORTB,0
        bsf PORTB,1
        bsf PORTB,2
        bsf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bcf PORTB,6
        bsf PORTB,7
        return
Show5
        bsf PORTB,0
        bsf PORTB,1
        bcf PORTB,2
        bsf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bcf PORTB,6
        bsf PORTB,7
        return
Show4
        bsf PORTB,0
        bsf PORTB,1
        bcf PORTB,2
        bcf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bcf PORTB,7
        return
Show3
        bcf PORTB,0
        bsf PORTB,1
        bcf PORTB,2
        bsf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bsf PORTB,7
        return
Show2
        bcf PORTB,0
        bsf PORTB,1
        bsf PORTB,2
        bsf PORTB,3
        bcf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bsf PORTB,7
        return
Show1
        bcf PORTB,0
        bcf PORTB,1
        bcf PORTB,2
        bcf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bcf PORTB,7
        return
Show0
        bsf PORTB,0
        bcf PORTB,1
        bsf PORTB,2
        bsf PORTB,3
        bsf PORTB,4
        bcf PORTB,5
        bsf PORTB,6
        bsf PORTB,7
        return
Delay
	movlw .156 ; 1
	movwf DELAY1 ; 1
	movlw .150 ; 1
	movwf DELAY2 ; 1
	movlw .14 ; 1
	movwf DELAY3 ; 1
	call delay3 ; 999717
	movlw .88 ; 1
	movwf DELAY1 ; 1
	call delay1 ; 269
	nop ; 1
	nop ; 1
	return ; 4

;Program
;=============================
Start
	goto Initialize
Initialize
	call chgbnk0 ; Go to bank 0	
	call chgbnk1 ; Go to bank 1
	clrf TRISB ; Make RB0-RB7 outputs (0 = output, 1 = input) 
	call chgbnk0 ; Go to bank 0
	clrf PORTB
	goto Cycle
Cycle
	call Show9
	call Delay
	call Show8     
        call Delay
	call Show7     
        call Delay
	call Show6     
        call Delay
	call Show5     
        call Delay
	call Show4     
        call Delay
	call Show3     
        call Delay
	call Show2     
        call Delay
	call Show1     
        call Delay
	call Show0     
        call Delay
	goto Cycle
	end
